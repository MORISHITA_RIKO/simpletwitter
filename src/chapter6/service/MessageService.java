package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId, String start, String end) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();
            Integer id = null;
            if(!StringUtils.isEmpty(userId)) {
              id = Integer.parseInt(userId);
            }
            String defaultStart = "2021/01/01 00:00:00";
            Date date = new Date();
            long time = date.getTime();
            Timestamp ts = new Timestamp(time);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            String defaultEnd = sdf.format(ts);

            if(!StringUtils.isEmpty(start)) {
            	start += " 00:00:00";
            } else {
            	start = defaultStart;
            }
            if(!StringUtils.isEmpty(end)) {
            	end += " 23:59:59";
            } else {
            	end = defaultEnd;
            }

            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, start, end);
            commit(connection);
            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(Message message) {
    	Connection connection = null;
        try {
            connection = getConnection();

            new MessageDao().delete(connection, message);
            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public Message select(int id) {
    	Connection connection = null;
        try {
            connection = getConnection();

            Message message = new MessageDao().select(connection, id);
            commit(connection);

            return message;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(Message message) {
        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().update(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}